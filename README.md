# RetroDB

Utilitários, hacks e gambiarras para ajudar na manutenção de um Banco de Dados para catálogo de artefatos de Retrocomputação.

## Abreviações

* WiP : *Work In Progress*
* RiP : *Research In Progress* 

## Introdução

*Things change*. Mas os dados não. Este repositório é um esforço para ajudar à manter vivos e atualizados alguns banco de dados famosos (ou nem tanto), e dar uma mãozinha para os *new kids on the block* .

Eventualmente novos *schemas* devem surgir para preencher lacunas. Mas um passo de cada vez, que isso dá tabalho pra burro!

## Serviços clientes

No momento, só o [service.retro.lisias.net](http://service.retro.lisias.net/db/)

## Artefatos neste respositório

### Suporte ao ZXDB do Einar Saukas

O Saukas está mantendo o ZXDB, uma base de dados que consolidou, atualizou e limpou a base de dados do World of Spectrum, o SPEX e o SPOT.

No começo, os scripts de criação e alimentação do DB suportava apenas o MariaDB/MySQL, e como eu sou Postgres *die hard*, comecei este trabalho para converter os scripts de MySQL para o Postgres.

Não é pouco trabalho. Além da sintaxe ser um pouco diferente (se bem que nada que umas REGEX marotas não resolvam), o Postgres é caxias e não abre mão das integridades na hora de subir massa de dados. 

Já o MySQL permite desativar todas as checagens com *pragmas*, e os *dumps* SQL se valem disso.

A solução que eu adotei foi a boa e velha **força bruta** :-) . Me vali de que o ZXDB não é tão grande assim e montei um script `Python` para "transpilar" os scripts para um formato que deixe o Postgres feliz, reordenando a criação das tabelas e da inserção das tuplas para não violar integridade referencial.

#### Pré-requisitos

* Postgresql
	* Tô usando o 9.6, mas deve funcionar de boa em qualquer versão depois da 9.0 (ou mesmo anteriores)
* Python 3.4
	* Mínimo.
	* 3.5 e 3.6 também funcionam.
	* Os anteriores devem dar pau, eu uso *features* da 3.4 cotidianamente.
* ZXDB.
	* Baixar [daqui](https://www.dropbox.com/sh/bgtoq6tdwropzzr/AAAuMt4OlA_RicOBgwQLopoMa/ZXDB?dl=0).

#### Configuração

* Clonar [isto](https://bitbucket.org/Lisias/retrodb/) aqui. :-)
* Baixar os dados para popular os DBs
	* ZXDB e descompactar o arquivo `ZXDB_latest_mysql.sql` em `./RetroDB/RAW/zxdb/` , onde `.` é o diretório onde você clonou este repo.
	* Opcional:
		- Baixar o [SPOT](http://www.users.globalnet.co.uk/~jg27paw4/spot-on/default.htm) e descompactar em `./RetroDB/RAW/ss/spot`
		- Baixar o SPEX (mesma URL) e descompactar em `./RetroDB/RAW/ss/spex`
		- Não é muito útil no momento, estes dois são RiP por enquanto.
* Configurar os DBMS':
	* Para o Postgres, assumo que o arquivo `/etc/postgres/sql/9.4/main/pg_hba.conf` contêm (trocar 9.4 pela versão instalada na sua box):
 
```
local  all  postgres           trust
local  all  all                trust
host   all  all  127.0.0.1/32  trust
host   all  all  ::1/128       trust
```
* 
	* Opcional MySQL:
		* Rodar `CREATE DATABASE zxdb` manualmente usando seu console SQL preferido.
			- Só necessário da primeira vez.
			- Um dia eu corrijo isso. :-)
* Configurar as ferramentas para rodar na sua *box*:
	* editar `db/zxdb_reparse_latest.py`
		* ajustar a linha 12 para apontar para o diretório root do repositório.
		* Yeah, eu sei que tá tosco. Um dia destes eu conserto isso. :-)
	* editar `zxdb_up_psql.py`
		* setar a variável `PSQL_CMD` apontando para o `psql` local. (linha 19 no momento)
		* Se você instalou o Postgres com usuário "root" diferente do padrão `postgres`, ajustar as linhas 25 e 26.
	* editar `zxdb_up_mysql.py` (opcional)
		* setar a variável `PASSWD` (linha 9) com a senha do root do seu mysql 
		* setar a variável `MYSQL_CMD` apontando para o `mysql` local. (linha 18 no momento)

#### Instruções

1. à partir do `.` (root do repositório clonado), executar `zxdb_transpiler_psql.py`
	1. Vá fazer um café, isso demora um pouco. :-)
	2. A ferramenta assume que o Postgres está configurado para aceitar conexões locais sem senha.
	3. Ver [Configuração](#Configuração) acima.

#### Operação

Num MacMini 2011, o processo não comeu mais que uns 550Mega de RAM e levou menos que 10 minutos para executar. Not bad.

Deve ser possível executar isso até num Raspberry Pi 2.

## Documentação Disponível

Em [docs](./docs) :

* [zxdb_model.mxm](./docs/zxdb_modem.mxm])
	+ DER do ZXDB feito no [Mogwai ERDesigner NG](http://mogwai.sourceforge.net/erdesignerng.html)
* [zxdb_model.png](./docs/zxdb_modem.png])
	+ Exportação para PNG (usado em http://service.retro.lisias.net/db/)
* weblocs:
	+ Links para o ZXDB. 

## Known Issues

Work in Progress. :-)
