-- Connect using root

CREATE ROLE "retrodb" WITH
	INHERIT
	NOLOGIN
;
COMMIT;

CREATE ROLE "retrodb-admin" WITH
	NOINHERIT
	CREATEDB
	CREATEROLE
	LOGIN
	IN ROLE "retrodb"
	PASSWORD '100senha'
;
GRANT "retrodb" TO "retrodb-admin" WITH ADMIN OPTION;
COMMIT;

DROP DATABASE IF EXISTS "retrodb";
CREATE DATABASE "retrodb"
	OWNER = "retrodb"
	TEMPLATE = template0
	ENCODING = 'UTF-8'
	lc_collate = 'en_GB.UTF-8'
	lc_ctype = 'en_GB.UTF-8'
;
COMMIT;

REVOKE ALL PRIVILEGES ON DATABASE "retrodb" FROM "retrodb";
GRANT CONNECT ON DATABASE "retrodb" to "retrodb";
GRANT ALL PRIVILEGES ON DATABASE "retrodb" TO "retrodb-admin" WITH GRANT OPTION;
COMMIT;

show server_encoding;
show client_encoding;
