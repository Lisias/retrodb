-- Connect using admin

CREATE ROLE "retrodb-manager" WITH
	NOINHERIT
	LOGIN
	IN ROLE "retrodb"
	PASSWORD '100senha'
;

CREATE ROLE "retrodb-consulta" WITH
	NOINHERIT
	LOGIN
	IN ROLE "retrodb"
	PASSWORD '100senha'
;
