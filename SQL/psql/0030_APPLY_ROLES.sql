-- Connect using admin

GRANT CONNECT, TEMPORARY
	ON DATABASE "retrodb"
	TO "retrodb-manager"
;
GRANT EXECUTE
	ON ALL FUNCTIONS IN SCHEMA "zxdb"
	TO "retrodb-manager"
;
GRANT USAGE
	ON SCHEMA "zxdb"
	TO "retrodb-manager"
;
GRANT SELECT, INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES, TRIGGER
	ON ALL TABLES IN SCHEMA "zxdb"
	TO "retrodb-manager"
;
GRANT EXECUTE
	ON ALL FUNCTIONS IN SCHEMA "wos"
	TO "retrodb-manager"
;
GRANT USAGE
	ON SCHEMA "wos"
	TO "retrodb-manager"
;
GRANT SELECT, INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES, TRIGGER
	ON ALL TABLES IN SCHEMA "wos"
	TO "retrodb-manager"
;
GRANT EXECUTE
	ON ALL FUNCTIONS IN SCHEMA "tmp"
	TO "retrodb-manager"
;
GRANT USAGE
	ON SCHEMA "tmp"
	TO "retrodb-manager"
;
GRANT SELECT, INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES, TRIGGER
	ON ALL TABLES IN SCHEMA "tmp"
	TO "retrodb-manager"
;


GRANT CONNECT
	ON DATABASE "retrodb"
	TO "retrodb-consulta"
;
GRANT EXECUTE
	ON ALL FUNCTIONS IN SCHEMA "zxdb"
	TO "retrodb-consulta"
;
GRANT SELECT
	ON ALL TABLES IN SCHEMA "zxdb"
	TO "retrodb-consulta"
;
GRANT USAGE
	ON SCHEMA "zxdb"
	TO "retrodb-consulta"
;
GRANT EXECUTE
	ON ALL FUNCTIONS IN SCHEMA "wos"
	TO "retrodb-consulta"
;
GRANT SELECT
	ON ALL TABLES IN SCHEMA "wos"
	TO "retrodb-consulta"
;
GRANT USAGE
	ON SCHEMA "wos"
	TO "retrodb-consulta"
;
GRANT EXECUTE
	ON ALL FUNCTIONS IN SCHEMA "tmp"
	TO "retrodb-consulta"
;
GRANT SELECT
	ON ALL TABLES IN SCHEMA "tmp"
	TO "retrodb-consulta"
;
GRANT USAGE
	ON SCHEMA "tmp"
	TO "retrodb-consulta"
;
