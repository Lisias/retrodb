import re, sys, enum

RE_HEADER = re.compile('^-- (.+?)\n')


class Values_Line:
	class STATE(enum.Enum):
		START = 0
		WHITESPC = 1
		VALUE = 2
		STRING = 3
		STRING_END = 4
		COMPLETE = 5
		TRAILLING = 6
		END = 9

	@staticmethod
	def parse(s:str) -> tuple:
		state = Values_Line.STATE.START
		len_s = len(s)
		r = list()
		v = None
		c = ''
		i = 0
		while Values_Line.STATE.END != state:
			if Values_Line.STATE.START == state:
				v = ''
				if i >= len_s:
					state = Values_Line.STATE.COMPLETE
					continue
				state = Values_Line.STATE.WHITESPC
			elif Values_Line.STATE.WHITESPC == state:
				c = s[i]
				i += 1
				if c not in [' ', '\t']: state = Values_Line.STATE.VALUE
			elif Values_Line.STATE.VALUE == state:
				if "'" == c:
					state = Values_Line.STATE.STRING
					continue
				v += c
				if i >= len_s:
					state = Values_Line.STATE.COMPLETE
					continue
				c = s[i]
				i += 1
				if ',' == c: state = Values_Line.STATE.COMPLETE
			elif Values_Line.STATE.STRING == state:
				v += c
				c = s[i]
				i += 1
				if "'" == c:
					state = Values_Line.STATE.STRING_END
					v += c
				if i >= len(s):
					state = Values_Line.STATE.COMPLETE
			elif Values_Line.STATE.STRING_END == state:
				if i >= len(s):
					state = Values_Line.STATE.COMPLETE
				c = s[i]
				i += 1
				if "'" == c:
					state = Values_Line.STATE.STRING
					continue
				state = Values_Line.STATE.COMPLETE
			elif Values_Line.STATE.COMPLETE == state:
				r.append(v)
				state = Values_Line.STATE.TRAILLING
			elif Values_Line.STATE.TRAILLING == state:
				if ',' == c:
					state = Values_Line.STATE.START if i < len_s else Values_Line.STATE.END
					continue
				if i >= len(s):
					state = Values_Line.STATE.END
					continue
				c = s[i]
				i += 1
		return tuple(r)


class DDL(object):
	'''
	classdocs
	'''

	RE_CREATE = re.compile('(-- Dumping structure[\S\s]+?)-- Dumping data')
	RE_CMD = re.compile('(CREATE [\S\s]+?\)).*?;')

	RE_TABLE_NAME = re.compile('CREATE TABLE .+ "(\S+?)" \(')
	RE_FIELD_NAME = re.compile('"(\S+?)" .+? AUTO_INCREMENT')
	RE_INDEX_FIELD_NAME = re.compile('  KEY [\S\s]+?\("(.+)"\).*?,')
	RE_SEQUENCE_VALUE = re.compile('AUTO_INCREMENT=(\d+);')

	class Table:
		def __init__(self, name, header, ddl, prefix, suffix):
			self.name = name
			self.header = header
			self.ddl = ddl
			self.prefix = prefix
			self.suffix = suffix

		def __hash__(self):
			return self.name.__hash__()

		def __eq__(self, other):
			if isinstance(other, str):
				return self.name == other
			elif isinstance(other, DDL):
				return self.name == other.name
			else:
				False

		def __str__(self):
			return "DDL{%s}" % (self.name,)

		def __repr__(self):
			return str(self.__dict__)

	def __init__(self, the_file : str):
		'''
		Constructor
		'''
		self.filename = the_file

	def __enter__(self):
		with open(self.filename, 'r', encoding='UTF-8') as f:
			ddls = self.make_ddls(f.read())
		self.list = self.build_dependencies(ddls)
		self.names = [o.name for o in self.list]
		self.dict = dict()
		for t in self.list:
			self.dict[t.name] = t
		return self

	def __exit__(self, exc_type, exc_val, exc_tb):
		pass

	def __iter__(self):
		class Iter:
			def __init__(self, data):
				self.i = 0
				self.data = data
				self.len = len(data)

			def __next__(self):
				if self.i >= self.len:
					raise StopIteration()
				r =  self.data[self.i]
				self.i += 1
				return r

		return Iter(self.list)

	@staticmethod
	def make_ddls(the_file: str):
		matches = DDL.RE_CREATE.findall(the_file)
		r = list()
		for ddl in matches:
			prefix, suffix = list(),list()

			header = RE_HEADER.search(ddl)
			header = header.group(1)

			ddl = ddl.replace('`', '"').replace("varchar(", "VARCHAR(").replace("char(", "CHAR(").replace('mediumtext', 'TEXT').replace("decimal", 'DECIMAL')
			ddl = ddl.replace('int(11)', 'INTEGER').replace('int(10)', 'INTEGER').replace('smallint(6)', 'SMALLINT').replace('tinyint(4)', 'SMALLINT').replace('tinyint(1)', 'SMALLINT').replace(' unsigned','')
			ddl = ddl.replace('COLLATE utf8_bin', '').replace(' ENGINE=InnoDB', '').replace(' DEFAULT CHARSET=utf8 COLLATE=utf8_bin', '')
			ddl = ddl.replace('"wos_', '"wos"."').replace('"wos"."special_id"', '"wos_special_id"')
			ddl = ddl.replace('"zxsr_', '"zxsr"."')
			ddl = ddl.replace('"tmp_', '"tmp"."').replace('"tmp"."alt_', '"tmp_alt_').replace('"tmp"."wos" VARCHAR', '"tmp_wos" VARCHAR').replace('"tmp"."file_', '"tmp_file_').replace('"tmp"."description" VARCHAR', '"tmp_description" VARCHAR').replace('"tmp"."icontype_', '"tmp_icontype_').replace('"tmp"."people_', '"tmp_people_').replace('"tmp"."wos_', '"tmp_wos_').replace('"tmp"."is_', '"tmp_is_').replace('"tmp"."alternate_', '"tmp_alternate_')

			if "AUTO_INCREMENT" in ddl:
				sequence_name, table_name, field_name, sequence_value = DDL.fetch_sequence_data(ddl)
				prefix += ('CREATE SEQUENCE "%s";' % (sequence_name,),);
				suffix += ('ALTER SEQUENCE "%s" OWNED BY "%s"."%s";' % (sequence_name, table_name, field_name),)
				suffix += ('ALTER SEQUENCE "%s" RESTART WITH %s;' % (sequence_name, sequence_value),)
				ddl = ddl.replace("AUTO_INCREMENT", "DEFAULT NEXTVAL('%s')" % (sequence_name,))
				ddl = ddl.replace('NEXTVAL(\'wos"."', 'NEXTVAL(\'wos.')

			if "  KEY" in ddl:
				indexes, ddl = DDL.fetch_indexes(ddl)
				suffix.extend(indexes)

			ddl = ddl.replace('UNIQUE KEY', 'UNIQUE').replace(' DEFAULT CHARSET=utf8 COLLATE=utf8_bin', '')
			ddl = re.sub(r'UNIQUE .+?\(', 'UNIQUE (', ddl)

			ddl = DDL.RE_CMD.search(ddl)
			ddl = ddl.group(1) + ';\n'

			m = DDL.RE_TABLE_NAME.search(ddl)
			if not m:
				raise Exception("no table name")
			table_name = m.group(1)

			d = DDL.Table(table_name, header, ddl, prefix, suffix)
			r.append(d)

		return r

	@staticmethod
	def fetch_sequence_data(ddl : str):
		m = DDL.RE_TABLE_NAME.search(ddl)
		if not m:
			raise Exception("no table name")
		tablename = m.group(1)

		m = DDL.RE_FIELD_NAME.search(ddl)
		if not m:
			raise Exception("no field name")
		fieldname = m.group(1)

		m = DDL.RE_SEQUENCE_VALUE.search(ddl)
		if not m:
			raise Exception("no field name")
		sequencevalue = m.group(1)

		return "%s_%s_seq" % (tablename, fieldname), tablename, fieldname, sequencevalue

	@staticmethod
	def fetch_indexes(ddl : str):
		r = list()

		m = DDL.RE_TABLE_NAME.search(ddl)
		if not m:
			raise Exception("no table name")
		tablename = m.group(1)

		m = DDL.RE_INDEX_FIELD_NAME.findall(ddl)
		for fieldname in m:
			r += (
					('CREATE INDEX "%s_%s_ndx" on "%s"("%s");' % (tablename.replace('wos"."', 'wos_'), fieldname.replace('","', ''), tablename, fieldname)).replace('"tmp"."files_', '"tmp_files_').replace('"tmp"."magscans_', '"tmp_magscans_')
				,)

		ddl = re.sub(r'  KEY .+?,\n', '', ddl)
		return r, ddl

	RE_CONSTRATINT_REFERENCES = re.compile('CONSTRAINT .+? REFERENCES "(.+?)" ')
	@staticmethod
	def build_dependencies_rec( tables:list, table):
		r = list()

		matches = DDL.RE_CONSTRATINT_REFERENCES.findall(table.ddl)

		for m in matches:
			if 0 == tables.count(m):
				continue

			i = tables.index(m)
			dependency = tables[i]
			tables.remove(dependency)
			r.extend(DDL.build_dependencies(tables, dependency))

		r.append(table)
		if (0 != len(tables)):
			r.extend(DDL.build_dependencies(tables, tables.pop()))
		return r

	@staticmethod
	def list_dependents(sorted : list, assorted : list):
		copy = assorted.copy()
		for d in copy:
			matches = DDL.RE_CONSTRATINT_REFERENCES.findall(d.ddl)

			all_there = True
			for m in matches:
				all_there &= m in sorted or m == d

			if all_there:
				sorted.append(d)
				assorted.remove(d)

	@staticmethod
	def build_dependencies(ddls:list):
		result = list()

		while 0 != len(ddls):
			DDL.list_dependents(result, ddls)

		return result


class DML(object):
	'''
	classdocs
	'''
	RE_POPULATE = re.compile('(-- Dumping data [\S\s]+?)\/\*\!40000 ALTER TABLE \S+? ENABLE KEYS')
	RE_TABLE_NAME = re.compile('INSERT INTO "(\S+?)" \(')

	TABLES_OF_INTEREST = {
			'labels' : ('from_id', 'owner_id'),
			'entries' : ('original_id',)
		}
	class Table:
		def __init__(self, name, header, fields, values):
			self._name = name
			self._header = header
			self._fields = fields
			self._values = values

		def __hash__(self):
			return self._name.__hash__()

		def __eq__(self, other):
			if isinstance(other, str):
				return self._name == other
			elif isinstance(other, DML):
				return self._name == other._name
			else:
				False

		def __str__(self):
			return "DML{%s}" % (self._name,)

		def __repr__(self):
			return str(self.__dict__)

		@property
		def name(self):
			return self._name

		@property
		def dml(self):
			class Generator:
				def __init__(self, parent):
					self.parent = parent

				@property
				def start(self):
					r = ("""-----
-- %s
----
""" % self.parent._header)
					r += 'INSERT INTO "%s" (%s) VALUES ' % ( self.parent._name, ', '.join(['"%s"'%f for f in self.parent._fields]) )
					return r
				@property
				def end(self):
					return '; -- %s : %d tuples' % (self.parent.name, len(self.parent._values))

				@property
				def values(self):
					class Iter:
						def __init__(self, data):
							self.i = 0
							self.data = data
							self.last = len(data)-1

						def __next__(self):
							if self.i > self.last:
								raise StopIteration()
							try:
								return self._string_it(self.i)
							finally:
								self.i += 1

						def __getitem__(self, i):
							if i > self.last:
								raise IndexError()
							return self._string_it(i)

						def _string_it(self, i):
							return '(%s)%s' %  (
									','.join([s for s in self.data[i]]),
									',' if i < self.last else ''
								)

					return Iter(self.parent._values)

				def __enter__(self):
					return self

				def __exit__(self, exc_type, exc_val, exc_tb):
					pass
			return Generator(self)

	def __init__(self, the_file : str):
		'''
		Constructor
		'''
		self.filename=the_file

	def __enter__(self):
		with open(self.filename, 'r', encoding='UTF-8') as f:
			self.dict = self.build_dml(f.read())
		self.list = [o[1] for o in self.dict.items()]
		self.names = [o.name for o in self.list]
		return self

	def __exit__(self, exc_type, exc_val, exc_tb):
		pass

	def __getitem__(self, i):
		return self.dict[i]

	def __iter__(self):
		class Iter:
			def __init__(self, data):
				self.i = 0
				self.data = data
				self.len = len(data)

			def __next__(self):
				if self.i >= self.len:
					raise StopIteration()
				r = self.data[self.i]
				self.i += 1
				return r

		return Iter(self.list)

	@staticmethod
	def build_dml(the_file:str):
		r = dict()

		matches = DML.RE_POPULATE.findall(the_file)
		for dml in matches:
			dml = dml.replace("\\'", "''").replace('`', '"')

			dml = re.sub(r'\/\*.+\*\/', '', dml)
			dml = dml.replace('"wos_', '"wos"."').replace('"wos"."special_id"', '"wos_special_id"')
			dml = dml.replace('"zxsr_', '"zxsr"."')
			dml = dml.replace('"tmp_', '"tmp"."').replace('"tmp"."alt_', '"tmp_alt_').replace('"tmp"."wos" VARCHAR', '"tmp_wos" VARCHAR').replace('"tmp"."file_', '"tmp_file_').replace('"tmp"."description" VARCHAR', '"tmp_description" VARCHAR').replace('"tmp"."icontype_', '"tmp_icontype_').replace('"tmp"."people_', '"tmp_people_').replace('"tmp"."wos_', '"tmp_wos_').replace('"tmp"."is_', '"tmp_is_')

			#DML only!
			dml = dml.replace('"tmp"."wos",', '"tmp_wos",').replace('"tmp"."description",', '"tmp_description",').replace('"tmp"."alternate_title",', '"tmp_alternate_title",')

			m = DML.RE_TABLE_NAME.search(dml)
			table_name = m.group(1)

			m = RE_HEADER.search(dml)
			header = m.group(1)

			dml = re.sub(r'^-- .+?\n', '', dml)
			dml = re.sub(r'^;\n', '', dml) # getting rid of redundant ';'s the last versions are coming with

			t = DML.Table(table_name, header,
					*DML.split_values_tuples(
						dml,
						DML.TABLES_OF_INTEREST[table_name] \
							if table_name in DML.TABLES_OF_INTEREST \
							else None
						)
				)

			r[table_name] = t

		return r

	@staticmethod
	def	sorting_key (ndx:int):
		return lambda t: int(t[ndx]) if t[ndx].isdigit() or ('-' == t[ndx][0] and t[ndx][1:].isdigit()) else t[ndx]
	@staticmethod
	def	sorting_keys (ndx:list):
		return lambda t: min([ int(t[n]) if t[n].isdigit() else sys.maxsize for n in ndx ])

	RE_INSERT_CMD = re.compile('(INSERT INTO .+? VALUES)')
	RE_FIELD_LIST = re.compile('INSERT INTO .*?\((.+?)\)+ VALUES')
	RE_DATA_LINES = re.compile('INSERT INTO .+? VALUES.*?\n(.+;\n)', flags=re.DOTALL)
	RE_DATA_LINES_ITEMS = re.compile('\t\((.+?)\)[,;]\n')
	@staticmethod
	def split_values_tuples(dml, interesting_fields):
		insert_cmd = DML.RE_INSERT_CMD.match(dml)
		if insert_cmd is None:
			raise Exception("RE_INSERT_CMD faiLure")

		field_list = DML.RE_FIELD_LIST.match(dml)
		if field_list is None:
			raise Exception("RE_FIELD_LIST faiLure")
		field_list = field_list.group(1).split(', ')
		field_list = tuple([i[1:-1] for i in field_list])

		data_lines = DML.RE_DATA_LINES.findall(dml)
		if data_lines is None:
			raise Exception("RE_DATA_LINES faiLure")

		data_lines = [ DML.RE_DATA_LINES_ITEMS.findall(i) for i in data_lines ][0]
		data_lines = [ Values_Line.parse(d) for d in data_lines ]

#		field_list_len = len(field_list)
# 		for data_line in data_lines:
# 			if len(data_line) != field_list_len:
# 				raise Exception("field_list_len faiLure")

		if interesting_fields is None:
			data_lines.sort(key = DML.sorting_key(0))
			return field_list, data_lines

		interesting_fields_ndx = [field_list.index(i) for i in interesting_fields if i in field_list]
		def fe(n):
			return lambda t : n == sum([ 1 if 'NULL' != t[i] else 0 for i in interesting_fields_ndx])
		def fne(n):
			return lambda t : n != sum([ 1 if 'NULL' != t[i] else 0 for i in interesting_fields_ndx])
		def fle(n):
			return lambda t : n < sum([ 1 if 'NULL' != t[i] else 0 for i in interesting_fields_ndx])

		result_lines = list(filter(fe(0), data_lines))
		result_lines.sort(key = DML.sorting_key(0))
		known_ids = set([ t[0] for t in result_lines])

		dirty_lines = list(filter(fne(0), data_lines))
		dirty_lines.sort(key = DML.sorting_key(0))
		while 0 != len(dirty_lines):
			l = len(dirty_lines)
			for dirty in dirty_lines:
				if all([ 'NULL' == t or t in known_ids for t in [ dirty[i] for i in interesting_fields_ndx] ]):
					dirty_lines.remove(dirty)
					result_lines.append(dirty)
					known_ids.add(dirty[0])
			if l == len(dirty_lines):
				raise "Dead Lock!!"

		return field_list, result_lines
