'''
Created on 12 de nov de 2016

@author: lisias
'''

import os, math

from db import DDL, DML

def PATH(relative : str) -> str:
	return os.path.join('/Users/lisias/code/net.lisias/net.lisias.retro/retrodb/', relative)

def run():
	the_file = PATH('RAW/zxdb/ZXDB_latest_mysql.sql')

	print("Parsing DDL", end='')
	with DDL(the_file) as ddl:
		with open(PATH('SQL/psql/zxdb/ZXDB_latest_0000_create.sql'), 'w', encoding='UTF-8') as f:
			f.write('SET search_path TO "zxdb";\n\n')

			for d in ddl:
				f.write ("""-----
-- %s
----
""" % d.header)

				if 0 != len(d.prefix):
					f.write("\n".join(d.prefix) + '\n')
				f.write("\n")
				f.write(d.ddl)
				if 0 != len(d.suffix):
					f.write("\n".join(d.suffix) + '\n')
				f.write('\n\n')
				print(".", end='')

		print(" ok!")
		tablenames = ddl.names # We will generate the DML following the exact order from the DDL.


	print("Parsing DML... ", end='')
	with DML(the_file) as dml:
		print("ok! Writing scripts", end='')
		n = 0
		width = math.floor(math.log10(len(tablenames)))+1
		for tablename in tablenames:
			n += 1
			fname = 'SQL/psql/zxdb/ZXDB_latest_0010_{i:0{w}d}_populate_{name:s}.sql'.format(i=n, w=width, name=tablename)
			with open(PATH(fname), 'w', encoding='UTF-8') as f:
				f.write('SET search_path TO "zxdb";\n\n')

				table = dml[tablename]
				with table.dml as d:
					i = 0
					f.write("%s\n" % d.start)
					for l in d.values:
						i += 1
						if 0 == i % 1000:
							f.write("\t%s\n" % l[:-1]) # Gambiarra para eliminar a ',' no fim do value.
							f.write("%s\n" % d.end)
							f.write("%s\n" % d.start)
						else:
							f.write("\t%s\n" % l)
				f.write('\n\n')
			print(".", end='')
		print(" ok!")

if __name__ == '__main__':
	run()
