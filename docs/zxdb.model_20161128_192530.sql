DROP TABLE publishers;
DROP TABLE specials;
DROP TABLE topicrefs;
DROP TABLE authors;
ALTER TABLE attachments DROP CONSTRAINT fk_wos_attachment_topicheader;
DROP TABLE topicheaders;
