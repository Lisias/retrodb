CREATE TABLE zxdb.toolfiles (
	id int4 NOT NULL,
	tool_id int4 NOT NULL,
	filetype_id int2 NOT NULL,
	file_link varchar(300) NOT NULL,
	file_date varchar(50) DEFAULT NULL::character varying,
	file_size int4,
	description varchar(100) DEFAULT NULL::character varying
);
ALTER TABLE zxdb.toolfiles ADD CONSTRAINT toolfiles_pkey PRIMARY KEY(id);
CREATE INDEX toolfiles_filetype_id_ndx ON zxdb.toolfiles (filetype_id);
CREATE INDEX toolfiles_tool_id_ndx ON zxdb.toolfiles (tool_id);
CREATE TABLE zxdb.websites (
	id int2 NOT NULL,
	name varchar(100) NOT NULL,
	link varchar(200) NOT NULL
);
ALTER TABLE zxdb.websites ADD CONSTRAINT websites_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX websites_link_key ON zxdb.websites (link);
CREATE UNIQUE INDEX websites_name_key ON zxdb.websites (name);
CREATE TABLE zxdb.booktypeins (
	entry_id int4 NOT NULL,
	book_id int4 NOT NULL,
	installment int2 NOT NULL DEFAULT '0'::smallint,
	volume int2 NOT NULL DEFAULT '0'::smallint,
	page int2 NOT NULL DEFAULT '0'::smallint
);
ALTER TABLE zxdb.booktypeins ADD CONSTRAINT booktypeins_pkey PRIMARY KEY(entry_id,book_id,installment,volume,page);
CREATE INDEX booktypeins_book_id_ndx ON zxdb.booktypeins (book_id);
CREATE TABLE zxdb.publicationtypes (
	id bpchar(1) NOT NULL,
	text varchar(50) NOT NULL
);
ALTER TABLE zxdb.publicationtypes ADD CONSTRAINT publicationtypes_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX publicationtypes_text_key ON zxdb.publicationtypes (text);
CREATE TABLE zxdb.labelfiles (
	id serial NOT NULL DEFAULT nextval('zxdb.labelfiles_id_seq'::regclass),
	filetype_id int2 NOT NULL,
	label_id int4 NOT NULL,
	file_link varchar(300) DEFAULT NULL::character varying,
	file_date varchar(50) DEFAULT NULL::character varying,
	file_size int4,
	description varchar(300) DEFAULT NULL::character varying
);
ALTER TABLE zxdb.labelfiles ADD CONSTRAINT labelfiles_pkey PRIMARY KEY(id);
CREATE INDEX labelfiles_filetype_id_ndx ON zxdb.labelfiles (filetype_id);
CREATE INDEX labelfiles_label_id_ndx ON zxdb.labelfiles (label_id);
CREATE TABLE zxdb.permissiontypes (
	id bpchar(1) NOT NULL,
	text varchar(50) NOT NULL
);
ALTER TABLE zxdb.permissiontypes ADD CONSTRAINT permissiontypes_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX permissiontypes_text_key ON zxdb.permissiontypes (text);
CREATE TABLE zxdb.authors (
	entry_id int4 NOT NULL,
	author_seq int2 NOT NULL DEFAULT '1'::smallint,
	label_id int4 NOT NULL,
	team_id int4
);
ALTER TABLE zxdb.authors ADD CONSTRAINT authors_pkey PRIMARY KEY(entry_id,author_seq);
CREATE UNIQUE INDEX authors_entry_id_label_id_key ON zxdb.authors (entry_id,label_id);
CREATE INDEX authors_label_id_ndx ON zxdb.authors (label_id);
CREATE INDEX authors_team_id_ndx ON zxdb.authors (team_id);
CREATE TABLE zxdb.entries (
	id int4 NOT NULL,
	title varchar(250) NOT NULL,
	original_id int4,
	is_mod int2 NOT NULL,
	is_xrated int2 NOT NULL,
	was_inspired int2,
	license_id int4,
	machinetype_id int2,
	max_players int2,
	turntype_id bpchar(1) DEFAULT NULL::bpchar,
	entrytype_id int2,
	publicationtype_id bpchar(1) DEFAULT NULL::bpchar,
	spanish_price varchar(50) DEFAULT NULL::character varying,
	microdrive_price varchar(50) DEFAULT NULL::character varying,
	disk_price varchar(50) DEFAULT NULL::character varying,
	cartridge_price varchar(50) DEFAULT NULL::character varying,
	availabletype_id bpchar(1) DEFAULT NULL::bpchar,
	known_errors text,
	comments varchar(5000) DEFAULT NULL::character varying,
	spot_comments varchar(1000) DEFAULT NULL::character varying,
	hardware_blurb varchar(1000) DEFAULT NULL::character varying,
	hardware_feature varchar(300) DEFAULT NULL::character varying,
	without_load_screen int2 NOT NULL,
	without_inlay int2 NOT NULL,
	hide_from_stp int2 NOT NULL,
	idiom_id bpchar(2) DEFAULT NULL::bpchar,
	book_isbn varchar(50) DEFAULT NULL::character varying,
	book_pages varchar(50) DEFAULT NULL::character varying,
	tmp_original_price_from_spot varchar(150) DEFAULT NULL::character varying
);
ALTER TABLE zxdb.entries ADD CONSTRAINT entries_pkey PRIMARY KEY(id);
CREATE INDEX entries_availabletype_id_ndx ON zxdb.entries (availabletype_id);
CREATE INDEX entries_entrytype_id_ndx ON zxdb.entries (entrytype_id);
CREATE INDEX entries_idiom_id_ndx ON zxdb.entries (idiom_id);
CREATE INDEX entries_license_id_ndx ON zxdb.entries (license_id);
CREATE INDEX entries_machinetype_id_ndx ON zxdb.entries (machinetype_id);
CREATE INDEX entries_original_id_ndx ON zxdb.entries (original_id);
CREATE INDEX entries_publicationtype_id_ndx ON zxdb.entries (publicationtype_id);
CREATE INDEX entries_turntype_id_ndx ON zxdb.entries (turntype_id);
CREATE TABLE zxdb.referencetypes (
	id int2 NOT NULL,
	text varchar(50) NOT NULL
);
ALTER TABLE zxdb.referencetypes ADD CONSTRAINT referencetypes_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX referencetypes_text_key ON zxdb.referencetypes (text);
CREATE TABLE zxdb.tmp_files (
	id int4 NOT NULL,
	filetype_id int2 NOT NULL,
	tmp_wos varchar(300) DEFAULT NULL::character varying,
	tmp_file_link varchar(300) DEFAULT NULL::character varying,
	tmp_file_size int4,
	tmp_file_date varchar(50) DEFAULT NULL::character varying,
	tmp_description varchar(500) DEFAULT NULL::character varying,
	tmp_icontype_id int2,
	tmp_people_key varchar(300) DEFAULT NULL::character varying,
	tmp_people_tag varchar(300) DEFAULT NULL::character varying
);
ALTER TABLE zxdb.tmp_files ADD CONSTRAINT tmp_files_pkey PRIMARY KEY(id);
CREATE INDEX tmp_files_filetype_id_ndx ON zxdb.tmp_files (filetype_id);
CREATE INDEX tmp_files_tmp_icontype_id_ndx ON zxdb.tmp_files (tmp_icontype_id);
CREATE TABLE zxdb.origintypes (
	id bpchar(1) NOT NULL,
	text varchar(50) NOT NULL
);
ALTER TABLE zxdb.origintypes ADD CONSTRAINT origintypes_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX origintypes_text_key ON zxdb.origintypes (text);
CREATE TABLE zxdb.tmp_magscans (
	id int4 NOT NULL,
	filetype_id int2 NOT NULL,
	magazine_id int2 NOT NULL,
	issue_id int4,
	page int2,
	file_link varchar(300) NOT NULL,
	file_date varchar(50) DEFAULT NULL::character varying,
	file_size int4,
	description varchar(300) DEFAULT NULL::character varying,
	tmp_icontype_id int2
);
ALTER TABLE zxdb.tmp_magscans ADD CONSTRAINT tmp_magscans_pkey PRIMARY KEY(id);
CREATE INDEX tmp_magscans_filetype_id_ndx ON zxdb.tmp_magscans (filetype_id);
CREATE INDEX tmp_magscans_issue_id_ndx ON zxdb.tmp_magscans (issue_id);
CREATE INDEX tmp_magscans_magazine_id_ndx ON zxdb.tmp_magscans (magazine_id);
CREATE INDEX tmp_magscans_tmp_icontype_id_ndx ON zxdb.tmp_magscans (tmp_icontype_id);
CREATE TABLE zxdb.extras (
	id serial NOT NULL DEFAULT nextval('zxdb.extras_id_seq'::regclass),
	filetype_id int2 NOT NULL,
	file_link varchar(300) NOT NULL,
	file_date varchar(50) DEFAULT NULL::character varying,
	file_size int4
);
ALTER TABLE zxdb.extras ADD CONSTRAINT extras_pkey PRIMARY KEY(id);
CREATE INDEX extras_filetype_id_ndx ON zxdb.extras (filetype_id);
CREATE TABLE zxdb.tmp_icontypes (
	id int2 NOT NULL,
	text varchar(50) NOT NULL
);
ALTER TABLE zxdb.tmp_icontypes ADD CONSTRAINT tmp_icontypes_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX tmp_icontypes_text_key ON zxdb.tmp_icontypes (text);
CREATE TABLE zxdb.tools (
	id int4 NOT NULL,
	platform_id int2 NOT NULL,
	title varchar(250) NOT NULL,
	authors varchar(300) DEFAULT NULL::character varying,
	link varchar(200) DEFAULT NULL::character varying,
	latest_date varchar(100) DEFAULT NULL::character varying,
	comments varchar(1000) DEFAULT NULL::character varying
);
ALTER TABLE zxdb.tools ADD CONSTRAINT tools_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX tools_platform_id_title_key ON zxdb.tools (platform_id,title);
CREATE TABLE zxdb.members (
	group_id int4 NOT NULL,
	entry_id int4 NOT NULL,
	series_seq int2
);
ALTER TABLE zxdb.members ADD CONSTRAINT members_pkey PRIMARY KEY(group_id,entry_id);
CREATE INDEX members_entry_id_ndx ON zxdb.members (entry_id);
CREATE TABLE zxdb.licenses (
	id int4 NOT NULL,
	name varchar(100) NOT NULL,
	licensetype_id bpchar(1) NOT NULL,
	license_link varchar(200) DEFAULT NULL::character varying
);
ALTER TABLE zxdb.licenses ADD CONSTRAINT licenses_pkey PRIMARY KEY(id);
CREATE INDEX licenses_licensetype_id_ndx ON zxdb.licenses (licensetype_id);
CREATE TABLE zxdb.aliases (
	id serial NOT NULL DEFAULT nextval('zxdb.aliases_id_seq'::regclass),
	entry_id int4 NOT NULL,
	release_seq int2 NOT NULL DEFAULT '0'::smallint,
	idiom_id bpchar(2) DEFAULT NULL::bpchar,
	title varchar(250) NOT NULL
);
ALTER TABLE zxdb.aliases ADD CONSTRAINT aliases_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX aliases_entry_id_release_seq_idiom_id_title_key ON zxdb.aliases (entry_id,release_seq,idiom_id,title);
CREATE INDEX aliases_entry_id_ndx ON zxdb.aliases (entry_id);
CREATE INDEX aliases_idiom_id_ndx ON zxdb.aliases (idiom_id);
CREATE TABLE zxdb.filetypes (
	id int2 NOT NULL,
	text varchar(50) NOT NULL
);
ALTER TABLE zxdb.filetypes ADD CONSTRAINT filetypes_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX filetypes_text_key ON zxdb.filetypes (text);
CREATE TABLE zxdb.platforms (
	id int2 NOT NULL,
	text varchar(50) NOT NULL
);
ALTER TABLE zxdb.platforms ADD CONSTRAINT platforms_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX platforms_text_key ON zxdb.platforms (text);
CREATE TABLE zxdb.authorings (
	entry_id int4 NOT NULL,
	util_id int4 NOT NULL
);
ALTER TABLE zxdb.authorings ADD CONSTRAINT authorings_pkey PRIMARY KEY(entry_id,util_id);
CREATE INDEX authorings_util_id_ndx ON zxdb.authorings (util_id);
CREATE TABLE zxdb.compilations (
	compilation_id int4 NOT NULL,
	entry_id int4 NOT NULL,
	tape_seq bpchar(1) NOT NULL,
	tape_side bpchar(1) NOT NULL,
	prog_seq int2 NOT NULL DEFAULT '1'::smallint,
	alias varchar(300) DEFAULT NULL::character varying,
	variationtype_id bpchar(1) NOT NULL
);
ALTER TABLE zxdb.compilations ADD CONSTRAINT compilations_pkey PRIMARY KEY(compilation_id,tape_seq,tape_side,prog_seq);
CREATE INDEX compilations_entry_id_ndx ON zxdb.compilations (entry_id);
CREATE INDEX compilations_variationtype_id_ndx ON zxdb.compilations (variationtype_id);
CREATE TABLE zxdb.roles (
	entry_id int4 NOT NULL,
	author_seq int2 NOT NULL,
	roletype_id bpchar(1) NOT NULL
);
ALTER TABLE zxdb.roles ADD CONSTRAINT roles_pkey PRIMARY KEY(entry_id,author_seq,roletype_id);
CREATE INDEX roles_roletype_id_ndx ON zxdb.roles (roletype_id);
CREATE TABLE zxdb.ports (
	id int4 NOT NULL,
	entry_id int4 NOT NULL,
	platform_id int2 NOT NULL,
	is_official int2,
	link_system varchar(200) DEFAULT NULL::character varying
);
ALTER TABLE zxdb.ports ADD CONSTRAINT ports_pkey PRIMARY KEY(id);
CREATE INDEX ports_entry_id_ndx ON zxdb.ports (entry_id);
CREATE INDEX ports_platform_id_ndx ON zxdb.ports (platform_id);
CREATE TABLE zxdb.labels (
	id int4 NOT NULL,
	name varchar(100) NOT NULL,
	tmp_alt_name_from_spot varchar(100) DEFAULT NULL::character varying,
	country_id bpchar(2) DEFAULT NULL::bpchar,
	country2_id bpchar(2) DEFAULT NULL::bpchar,
	from_id int4,
	owner_id int4,
	was_renamed int2,
	is_deceased int2,
	tribute varchar(200) DEFAULT NULL::character varying,
	link_wikipedia varchar(200) DEFAULT NULL::character varying,
	link_site varchar(200) DEFAULT NULL::character varying,
	link_promo varchar(200) DEFAULT NULL::character varying,
	is_company int2
);
ALTER TABLE zxdb.labels ADD CONSTRAINT labels_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX labels_name_key ON zxdb.labels (name);
CREATE INDEX labels_country2_id_ndx ON zxdb.labels (country2_id);
CREATE INDEX labels_country_id_ndx ON zxdb.labels (country_id);
CREATE INDEX labels_from_id_ndx ON zxdb.labels (from_id);
CREATE INDEX labels_owner_id_ndx ON zxdb.labels (owner_id);
CREATE TABLE zxdb.tmp_utilities (
	id int4 NOT NULL,
	filetype_id int2 NOT NULL,
	file_link varchar(300) DEFAULT NULL::character varying,
	file_date varchar(50) DEFAULT NULL::character varying,
	file_size int4,
	tmp_wos varchar(300) DEFAULT NULL::character varying
);
ALTER TABLE zxdb.tmp_utilities ADD CONSTRAINT tmp_utilities_pkey PRIMARY KEY(id);
CREATE INDEX tmp_utilities_filetype_id_ndx ON zxdb.tmp_utilities (filetype_id);
CREATE TABLE zxdb.controltypes (
	id bpchar(1) NOT NULL,
	text varchar(50) NOT NULL
);
ALTER TABLE zxdb.controltypes ADD CONSTRAINT controltypes_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX controltypes_text_key ON zxdb.controltypes (text);
CREATE TABLE zxdb.magrefs (
	id serial NOT NULL DEFAULT nextval('zxdb.magrefs_id_seq'::regclass),
	referencetype_id int2 NOT NULL,
	entry_id int4,
	label_id int4,
	issue_id int4 NOT NULL,
	page int2 NOT NULL DEFAULT '0'::smallint,
	link varchar(300) DEFAULT NULL::character varying,
	feature_id int4 NOT NULL DEFAULT 0
);
ALTER TABLE zxdb.magrefs ADD CONSTRAINT magrefs_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX magrefs_referencetype_id_entry_id_issue_id_page_feature_id_key ON zxdb.magrefs (referencetype_id,entry_id,issue_id,page,feature_id);
CREATE UNIQUE INDEX magrefs_referencetype_id_label_id_issue_id_page_feature_id_key ON zxdb.magrefs (referencetype_id,label_id,issue_id,page,feature_id);
CREATE INDEX magrefs_entry_id_ndx ON zxdb.magrefs (entry_id);
CREATE INDEX magrefs_feature_id_ndx ON zxdb.magrefs (feature_id);
CREATE INDEX magrefs_issue_id_ndx ON zxdb.magrefs (issue_id);
CREATE INDEX magrefs_label_id_ndx ON zxdb.magrefs (label_id);
CREATE TABLE zxdb.licensetypes (
	id bpchar(1) NOT NULL,
	text varchar(50) NOT NULL
);
ALTER TABLE zxdb.licensetypes ADD CONSTRAINT licensetypes_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX licensetypes_text_key ON zxdb.licensetypes (text);
CREATE TABLE zxdb.variationtypes (
	id bpchar(1) NOT NULL,
	text varchar(50) NOT NULL
);
ALTER TABLE zxdb.variationtypes ADD CONSTRAINT variationtypes_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX variationtypes_text_key ON zxdb.variationtypes (text);
CREATE TABLE zxdb.nvgs (
	id int4 NOT NULL,
	title varchar(250) NOT NULL,
	file_link varchar(300) DEFAULT NULL::character varying,
	file_date varchar(50) DEFAULT NULL::character varying,
	file_size int4,
	description varchar(500) DEFAULT NULL::character varying,
	url varchar(100) DEFAULT NULL::character varying
);
ALTER TABLE zxdb.nvgs ADD CONSTRAINT nvgs_pkey PRIMARY KEY(id);
CREATE TABLE zxdb.entrytypes (
	id int2 NOT NULL,
	text varchar(50) NOT NULL
);
ALTER TABLE zxdb.entrytypes ADD CONSTRAINT entrytypes_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX entrytypes_text_key ON zxdb.entrytypes (text);
CREATE TABLE zxdb.groups (
	id int4 NOT NULL,
	name varchar(100) NOT NULL,
	link varchar(200) DEFAULT NULL::character varying,
	description varchar(5000) DEFAULT NULL::character varying,
	grouptype_id bpchar(1) DEFAULT NULL::bpchar
);
ALTER TABLE zxdb.groups ADD CONSTRAINT groups_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX groups_name_grouptype_id_key ON zxdb.groups (name,grouptype_id);
CREATE INDEX groups_grouptype_id_ndx ON zxdb.groups (grouptype_id);
CREATE TABLE zxdb.scores (
	website_id int2 NOT NULL,
	entry_id int4 NOT NULL,
	score numeric(5, 2) NOT NULL,
	votes int4 NOT NULL
);
ALTER TABLE zxdb.scores ADD CONSTRAINT scores_pkey PRIMARY KEY(website_id,entry_id);
CREATE INDEX scores_entry_id_ndx ON zxdb.scores (entry_id);
CREATE TABLE zxdb.schemetypes (
	id bpchar(2) NOT NULL,
	text varchar(50) NOT NULL
);
ALTER TABLE zxdb.schemetypes ADD CONSTRAINT schemetypes_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX schemetypes_text_key ON zxdb.schemetypes (text);
CREATE TABLE zxdb.permissions (
	website_id int2 NOT NULL,
	label_id int4 NOT NULL,
	permissiontype_id bpchar(1) DEFAULT NULL::bpchar,
	text varchar(300) DEFAULT NULL::character varying
);
ALTER TABLE zxdb.permissions ADD CONSTRAINT permissions_pkey PRIMARY KEY(website_id,label_id);
CREATE INDEX permissions_label_id_ndx ON zxdb.permissions (label_id);
CREATE INDEX permissions_permissiontype_id_ndx ON zxdb.permissions (permissiontype_id);
CREATE TABLE zxdb.availabletypes (
	id bpchar(1) NOT NULL,
	text varchar(50) NOT NULL
);
ALTER TABLE zxdb.availabletypes ADD CONSTRAINT availabletypes_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX availabletypes_text_key ON zxdb.availabletypes (text);
CREATE TABLE zxdb.grouptypes (
	id bpchar(1) NOT NULL,
	text varchar(50) NOT NULL
);
ALTER TABLE zxdb.grouptypes ADD CONSTRAINT grouptypes_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX grouptypes_text_key ON zxdb.grouptypes (text);
CREATE TABLE zxdb.interviews (
	label_id int4 NOT NULL,
	title varchar(200) DEFAULT NULL::character varying,
	link varchar(200) NOT NULL,
	idiom_id bpchar(2) DEFAULT NULL::bpchar,
	interviewer varchar(200) DEFAULT NULL::character varying,
	interview_year int2
);
ALTER TABLE zxdb.interviews ADD CONSTRAINT interviews_pkey PRIMARY KEY(label_id,link);
CREATE INDEX interviews_idiom_id_ndx ON zxdb.interviews (idiom_id);
CREATE TABLE zxdb.magazines (
	id int2 NOT NULL,
	name varchar(100) NOT NULL
);
ALTER TABLE zxdb.magazines ADD CONSTRAINT magazines_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX magazines_name_key ON zxdb.magazines (name);
CREATE TABLE zxdb.hosts (
	id int2 NOT NULL,
	title varchar(150) NOT NULL,
	link varchar(150) NOT NULL,
	admin_name varchar(150) NOT NULL,
	admin_email varchar(150) NOT NULL,
	magazine_id int2 NOT NULL
);
ALTER TABLE zxdb.hosts ADD CONSTRAINT hosts_pkey PRIMARY KEY(id);
CREATE INDEX hosts_magazine_id_ndx ON zxdb.hosts (magazine_id);
CREATE TABLE zxdb.idioms (
	id bpchar(2) NOT NULL,
	text varchar(100) NOT NULL
);
ALTER TABLE zxdb.idioms ADD CONSTRAINT idioms_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX idioms_text_key ON zxdb.idioms (text);
CREATE TABLE zxdb.roletypes (
	id bpchar(1) NOT NULL,
	text varchar(50) NOT NULL
);
ALTER TABLE zxdb.roletypes ADD CONSTRAINT roletypes_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX roletypes_text_key ON zxdb.roletypes (text);
CREATE TABLE zxdb.machinetypes (
	id int2 NOT NULL,
	text varchar(50) NOT NULL
);
ALTER TABLE zxdb.machinetypes ADD CONSTRAINT machinetypes_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX machinetypes_text_key ON zxdb.machinetypes (text);
CREATE TABLE zxdb.countries (
	id bpchar(2) NOT NULL,
	text varchar(50) NOT NULL
);
ALTER TABLE zxdb.countries ADD CONSTRAINT countries_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX countries_text_key ON zxdb.countries (text);
CREATE TABLE zxdb.licensors (
	license_id int4 NOT NULL,
	label_id int4 NOT NULL
);
ALTER TABLE zxdb.licensors ADD CONSTRAINT licensors_pkey PRIMARY KEY(license_id,label_id);
CREATE INDEX licensors_label_id_ndx ON zxdb.licensors (label_id);
CREATE TABLE zxdb.relatedlinks (
	website_id int2 NOT NULL,
	entry_id int4 NOT NULL,
	link varchar(200) NOT NULL
);
ALTER TABLE zxdb.relatedlinks ADD CONSTRAINT relatedlinks_pkey PRIMARY KEY(website_id,entry_id);
CREATE INDEX relatedlinks_entry_id_ndx ON zxdb.relatedlinks (entry_id);
CREATE TABLE zxdb.downloads (
	id serial NOT NULL DEFAULT nextval('zxdb.downloads_id_seq'::regclass),
	entry_id int4 NOT NULL,
	release_seq int2 NOT NULL DEFAULT '0'::smallint,
	idiom_id bpchar(2) DEFAULT NULL::bpchar,
	file_link varchar(300) DEFAULT NULL::character varying,
	file_date varchar(50) DEFAULT NULL::character varying,
	file_size int4,
	filetype_id int2 NOT NULL,
	is_demo int2 NOT NULL,
	schemetype_id bpchar(2) DEFAULT NULL::bpchar,
	machinetype_id int2,
	description varchar(100) DEFAULT NULL::character varying,
	file_code varchar(50) DEFAULT NULL::character varying,
	file_barcode varchar(50) DEFAULT NULL::character varying,
	file_dl varchar(150) DEFAULT NULL::character varying,
	origintype_id bpchar(1) NOT NULL,
	release_year int2,
	tmp_alternate_title varchar(300) DEFAULT NULL::character varying,
	subtitle varchar(300) DEFAULT NULL::character varying,
	tmp_icontype_id int2
);
ALTER TABLE zxdb.downloads ADD CONSTRAINT downloads_pkey PRIMARY KEY(id);
CREATE INDEX downloads_entry_id_ndx ON zxdb.downloads (entry_id);
CREATE INDEX downloads_entry_idrelease_seq_ndx ON zxdb.downloads (entry_id,release_seq);
CREATE INDEX downloads_filetype_id_ndx ON zxdb.downloads (filetype_id);
CREATE INDEX downloads_idiom_id_ndx ON zxdb.downloads (idiom_id);
CREATE INDEX downloads_machinetype_id_ndx ON zxdb.downloads (machinetype_id);
CREATE INDEX downloads_origintype_id_ndx ON zxdb.downloads (origintype_id);
CREATE INDEX downloads_schemetype_id_ndx ON zxdb.downloads (schemetype_id);
CREATE INDEX downloads_tmp_icontype_id_ndx ON zxdb.downloads (tmp_icontype_id);
CREATE TABLE zxdb.controls (
	entry_id int4 NOT NULL,
	controltype_id bpchar(1) NOT NULL
);
ALTER TABLE zxdb.controls ADD CONSTRAINT controls_pkey PRIMARY KEY(entry_id,controltype_id);
CREATE INDEX controls_controltype_id_ndx ON zxdb.controls (controltype_id);
CREATE TABLE zxdb.turntypes (
	id bpchar(1) NOT NULL,
	text varchar(50) NOT NULL
);
ALTER TABLE zxdb.turntypes ADD CONSTRAINT turntypes_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX turntypes_text_key ON zxdb.turntypes (text);
CREATE TABLE zxdb.remakes (
	id int4 NOT NULL,
	entry_id int4 NOT NULL,
	title varchar(300) NOT NULL,
	link varchar(200) NOT NULL,
	file_date varchar(50) DEFAULT NULL::character varying,
	file_size int4,
	authors varchar(300) DEFAULT NULL::character varying,
	platforms varchar(200) DEFAULT NULL::character varying,
	remake_years varchar(100) DEFAULT NULL::character varying,
	remake_status varchar(1000) DEFAULT NULL::character varying
);
ALTER TABLE zxdb.remakes ADD CONSTRAINT remakes_pkey PRIMARY KEY(id);
CREATE INDEX remakes_entry_id_ndx ON zxdb.remakes (entry_id);
CREATE TABLE zxdb.releases (
	entry_id int4 NOT NULL,
	release_seq int2 NOT NULL DEFAULT '0'::smallint,
	release_year int2,
	release_price varchar(150) DEFAULT NULL::character varying,
	budget_price varchar(150) DEFAULT NULL::character varying
);
ALTER TABLE zxdb.releases ADD CONSTRAINT releases_pkey PRIMARY KEY(entry_id,release_seq);
CREATE TABLE zxdb.features (
	id int4 NOT NULL,
	name varchar(150) NOT NULL,
	version int2 NOT NULL DEFAULT '0'::smallint,
	label_id int4,
	host_id int2,
	tmp_wos_publisher_id int4,
	tmp_is_author int2 NOT NULL DEFAULT '0'::smallint,
	tmp_is_attachment int2 NOT NULL DEFAULT '0'::smallint
);
ALTER TABLE zxdb.features ADD CONSTRAINT features_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX features_name_version_label_id_tmp_is_author_key ON zxdb.features (name,version,label_id,tmp_is_author);
CREATE INDEX features_host_id_ndx ON zxdb.features (host_id);
CREATE INDEX features_label_id_ndx ON zxdb.features (label_id);
CREATE INDEX features_tmp_wos_publisher_id_ndx ON zxdb.features (tmp_wos_publisher_id);
CREATE TABLE zxdb.magfiles (
	id serial NOT NULL DEFAULT nextval('zxdb.magfiles_id_seq'::regclass),
	filetype_id int2 NOT NULL,
	issue_id int4 NOT NULL,
	page int2,
	file_link varchar(300) NOT NULL,
	file_date varchar(50) DEFAULT NULL::character varying,
	file_size int4,
	description varchar(300) DEFAULT NULL::character varying
);
ALTER TABLE zxdb.magfiles ADD CONSTRAINT magfiles_pkey PRIMARY KEY(id);
CREATE INDEX magfiles_filetype_id_ndx ON zxdb.magfiles (filetype_id);
CREATE INDEX magfiles_issue_id_ndx ON zxdb.magfiles (issue_id);
CREATE TABLE zxdb.issues (
	id serial NOT NULL DEFAULT nextval('zxdb.issues_id_seq'::regclass),
	magazine_id int2 NOT NULL,
	date_year int2,
	date_month int2,
	date_day int2,
	volume int2,
	number int2,
	special varchar(100) DEFAULT NULL::character varying
);
ALTER TABLE zxdb.issues ADD CONSTRAINT issues_pkey PRIMARY KEY(id);
CREATE INDEX issues_magazine_id_ndx ON zxdb.issues (magazine_id);
CREATE TABLE zxdb.publishers (
	entry_id int4 NOT NULL,
	release_seq int2 NOT NULL DEFAULT '0'::smallint,
	label_id int4 NOT NULL,
	publisher_seq int2 NOT NULL DEFAULT '1'::smallint
);
ALTER TABLE zxdb.publishers ADD CONSTRAINT publishers_pkey PRIMARY KEY(entry_id,release_seq,label_id);
CREATE UNIQUE INDEX publishers_entry_id_release_seq_publisher_seq_key ON zxdb.publishers (entry_id,release_seq,publisher_seq);
CREATE INDEX publishers_label_id_ndx ON zxdb.publishers (label_id);
ALTER TABLE zxdb.toolfiles ADD CONSTRAINT fk_toolfile_filetype FOREIGN KEY (filetype_id) REFERENCES zxdb.filetypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.toolfiles ADD CONSTRAINT fk_toolfile_tool FOREIGN KEY (tool_id) REFERENCES zxdb.tools(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.booktypeins ADD CONSTRAINT fk_booktypein_book FOREIGN KEY (book_id) REFERENCES zxdb.entries(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.booktypeins ADD CONSTRAINT fk_booktypein_entry FOREIGN KEY (entry_id) REFERENCES zxdb.entries(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.labelfiles ADD CONSTRAINT fk_labelfile_type FOREIGN KEY (filetype_id) REFERENCES zxdb.filetypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.labelfiles ADD CONSTRAINT fk_labelfile_label FOREIGN KEY (label_id) REFERENCES zxdb.labels(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.authors ADD CONSTRAINT fk_author_entry FOREIGN KEY (entry_id) REFERENCES zxdb.entries(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.authors ADD CONSTRAINT fk_author_label FOREIGN KEY (label_id) REFERENCES zxdb.labels(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.authors ADD CONSTRAINT fk_author_team FOREIGN KEY (team_id) REFERENCES zxdb.labels(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.entries ADD CONSTRAINT fk_entry_availabletype FOREIGN KEY (availabletype_id) REFERENCES zxdb.availabletypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.entries ADD CONSTRAINT fk_entry_original FOREIGN KEY (original_id) REFERENCES zxdb.entries(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.entries ADD CONSTRAINT fk_entry_entrytype FOREIGN KEY (entrytype_id) REFERENCES zxdb.entrytypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.entries ADD CONSTRAINT fk_entry_idiom FOREIGN KEY (idiom_id) REFERENCES zxdb.idioms(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.entries ADD CONSTRAINT fk_entry_license FOREIGN KEY (license_id) REFERENCES zxdb.licenses(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.entries ADD CONSTRAINT fk_entry_machinetype FOREIGN KEY (machinetype_id) REFERENCES zxdb.machinetypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.entries ADD CONSTRAINT fk_entry_publicationtype FOREIGN KEY (publicationtype_id) REFERENCES zxdb.publicationtypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.entries ADD CONSTRAINT fk_entry_turntype FOREIGN KEY (turntype_id) REFERENCES zxdb.turntypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.tmp_files ADD CONSTRAINT fk_tmp_file_filetype FOREIGN KEY (filetype_id) REFERENCES zxdb.filetypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.tmp_files ADD CONSTRAINT fk_tmp_file_tmp_icontype FOREIGN KEY (tmp_icontype_id) REFERENCES zxdb.tmp_icontypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.tmp_magscans ADD CONSTRAINT fk_tmp_magscan_filetype FOREIGN KEY (filetype_id) REFERENCES zxdb.filetypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.tmp_magscans ADD CONSTRAINT fk_tmp_magscan_issue FOREIGN KEY (issue_id) REFERENCES zxdb.issues(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.tmp_magscans ADD CONSTRAINT fk_tmp_magscan_magazine FOREIGN KEY (magazine_id) REFERENCES zxdb.magazines(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.tmp_magscans ADD CONSTRAINT fk_tmp_magscan_icontype FOREIGN KEY (tmp_icontype_id) REFERENCES zxdb.tmp_icontypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.extras ADD CONSTRAINT fk_extra_filetype FOREIGN KEY (filetype_id) REFERENCES zxdb.filetypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.tools ADD CONSTRAINT fk_tool_platform FOREIGN KEY (platform_id) REFERENCES zxdb.platforms(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.members ADD CONSTRAINT fk_member_entry FOREIGN KEY (entry_id) REFERENCES zxdb.entries(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.members ADD CONSTRAINT fk_member_group FOREIGN KEY (group_id) REFERENCES zxdb.groups(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.licenses ADD CONSTRAINT fk_license_type FOREIGN KEY (licensetype_id) REFERENCES zxdb.licensetypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.aliases ADD CONSTRAINT fk_alias_entry FOREIGN KEY (entry_id) REFERENCES zxdb.entries(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.aliases ADD CONSTRAINT fk_alias_idiom FOREIGN KEY (idiom_id) REFERENCES zxdb.idioms(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.aliases ADD CONSTRAINT fk_alias_release FOREIGN KEY (entry_id,release_seq) REFERENCES zxdb.releases(entry_id,release_seq) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.authorings ADD CONSTRAINT fk_authoring_entry FOREIGN KEY (entry_id) REFERENCES zxdb.entries(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.authorings ADD CONSTRAINT fk_authoring_util FOREIGN KEY (util_id) REFERENCES zxdb.entries(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.compilations ADD CONSTRAINT fk_compilation_compilation FOREIGN KEY (compilation_id) REFERENCES zxdb.entries(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.compilations ADD CONSTRAINT fk_compilation_entry FOREIGN KEY (entry_id) REFERENCES zxdb.entries(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.compilations ADD CONSTRAINT fk_compilation_variationtype FOREIGN KEY (variationtype_id) REFERENCES zxdb.variationtypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.roles ADD CONSTRAINT fk_role_author FOREIGN KEY (entry_id,author_seq) REFERENCES zxdb.authors(entry_id,author_seq) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.roles ADD CONSTRAINT fk_role_entry FOREIGN KEY (entry_id) REFERENCES zxdb.entries(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.roles ADD CONSTRAINT fk_role_type FOREIGN KEY (roletype_id) REFERENCES zxdb.roletypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.ports ADD CONSTRAINT fk_port_entry FOREIGN KEY (entry_id) REFERENCES zxdb.entries(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.ports ADD CONSTRAINT fk_port_platform FOREIGN KEY (platform_id) REFERENCES zxdb.platforms(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.labels ADD CONSTRAINT fk_label_country FOREIGN KEY (country_id) REFERENCES zxdb.countries(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.labels ADD CONSTRAINT fk_label_country2 FOREIGN KEY (country2_id) REFERENCES zxdb.countries(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.labels ADD CONSTRAINT fk_label_from FOREIGN KEY (from_id) REFERENCES zxdb.labels(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.labels ADD CONSTRAINT fk_label_owner FOREIGN KEY (owner_id) REFERENCES zxdb.labels(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.tmp_utilities ADD CONSTRAINT fk_tmp_utility_filetype FOREIGN KEY (filetype_id) REFERENCES zxdb.filetypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.magrefs ADD CONSTRAINT fk_magref_entry FOREIGN KEY (entry_id) REFERENCES zxdb.entries(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.magrefs ADD CONSTRAINT fk_magref_feature FOREIGN KEY (feature_id) REFERENCES zxdb.features(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.magrefs ADD CONSTRAINT fk_magref_issue FOREIGN KEY (issue_id) REFERENCES zxdb.issues(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.magrefs ADD CONSTRAINT fk_magref_label FOREIGN KEY (label_id) REFERENCES zxdb.labels(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.magrefs ADD CONSTRAINT fk_magref_type FOREIGN KEY (referencetype_id) REFERENCES zxdb.referencetypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.groups ADD CONSTRAINT fk_group_type FOREIGN KEY (grouptype_id) REFERENCES zxdb.grouptypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.scores ADD CONSTRAINT fk_score_entry FOREIGN KEY (entry_id) REFERENCES zxdb.entries(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.scores ADD CONSTRAINT fk_score_website FOREIGN KEY (website_id) REFERENCES zxdb.websites(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.permissions ADD CONSTRAINT fk_permission_label FOREIGN KEY (label_id) REFERENCES zxdb.labels(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.permissions ADD CONSTRAINT fk_permission_type FOREIGN KEY (permissiontype_id) REFERENCES zxdb.permissiontypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.permissions ADD CONSTRAINT fk_permission_website FOREIGN KEY (website_id) REFERENCES zxdb.websites(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.interviews ADD CONSTRAINT fk_interview_idiom FOREIGN KEY (idiom_id) REFERENCES zxdb.idioms(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.interviews ADD CONSTRAINT fk_interview_label FOREIGN KEY (label_id) REFERENCES zxdb.labels(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.hosts ADD CONSTRAINT fk_host_magazine FOREIGN KEY (magazine_id) REFERENCES zxdb.magazines(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.licensors ADD CONSTRAINT fk_licensor_label FOREIGN KEY (label_id) REFERENCES zxdb.labels(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.licensors ADD CONSTRAINT fk_licensor_license FOREIGN KEY (license_id) REFERENCES zxdb.licenses(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.relatedlinks ADD CONSTRAINT fk_relatedlink_entry FOREIGN KEY (entry_id) REFERENCES zxdb.entries(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.relatedlinks ADD CONSTRAINT fk_relatedlink_website FOREIGN KEY (website_id) REFERENCES zxdb.websites(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.downloads ADD CONSTRAINT fk_download_entry FOREIGN KEY (entry_id) REFERENCES zxdb.entries(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.downloads ADD CONSTRAINT fk_download_filetype FOREIGN KEY (filetype_id) REFERENCES zxdb.filetypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.downloads ADD CONSTRAINT fk_download_idiom FOREIGN KEY (idiom_id) REFERENCES zxdb.idioms(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.downloads ADD CONSTRAINT fk_download_machinetype FOREIGN KEY (machinetype_id) REFERENCES zxdb.machinetypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.downloads ADD CONSTRAINT fk_download_origintype FOREIGN KEY (origintype_id) REFERENCES zxdb.origintypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.downloads ADD CONSTRAINT fk_download_release FOREIGN KEY (entry_id,release_seq) REFERENCES zxdb.releases(entry_id,release_seq) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.downloads ADD CONSTRAINT fk_download_schemetype FOREIGN KEY (schemetype_id) REFERENCES zxdb.schemetypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.downloads ADD CONSTRAINT fk_download_tmp_icontype FOREIGN KEY (tmp_icontype_id) REFERENCES zxdb.tmp_icontypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.controls ADD CONSTRAINT fk_control_type FOREIGN KEY (controltype_id) REFERENCES zxdb.controltypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.controls ADD CONSTRAINT fk_control_entry FOREIGN KEY (entry_id) REFERENCES zxdb.entries(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.remakes ADD CONSTRAINT fk_remake_entry FOREIGN KEY (entry_id) REFERENCES zxdb.entries(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.releases ADD CONSTRAINT fk_release_entry FOREIGN KEY (entry_id) REFERENCES zxdb.entries(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.features ADD CONSTRAINT fk_feature_host FOREIGN KEY (host_id) REFERENCES zxdb.hosts(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.features ADD CONSTRAINT fk_feature_label FOREIGN KEY (label_id) REFERENCES zxdb.labels(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.magfiles ADD CONSTRAINT fk_magfile_type FOREIGN KEY (filetype_id) REFERENCES zxdb.filetypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.magfiles ADD CONSTRAINT fk_magfile_issue FOREIGN KEY (issue_id) REFERENCES zxdb.issues(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.issues ADD CONSTRAINT fk_issue_magazine FOREIGN KEY (magazine_id) REFERENCES zxdb.magazines(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.publishers ADD CONSTRAINT fk_publisher_entry FOREIGN KEY (entry_id) REFERENCES zxdb.entries(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.publishers ADD CONSTRAINT fk_publisher_label FOREIGN KEY (label_id) REFERENCES zxdb.labels(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE zxdb.publishers ADD CONSTRAINT fk_publisher_release FOREIGN KEY (entry_id,release_seq) REFERENCES zxdb.releases(entry_id,release_seq) ON DELETE NO ACTION ON UPDATE NO ACTION;
CREATE TABLE wos.attachments (
	id int4 NOT NULL,
	topicheaders_id int4,
	number numeric(4, 1) NOT NULL,
	feature_id int4
);
ALTER TABLE wos.attachments ADD CONSTRAINT attachments_pkey PRIMARY KEY(id);
CREATE INDEX wos_attachments_feature_id_ndx ON wos.attachments (feature_id);
CREATE INDEX wos_attachments_topicheaders_id_ndx ON wos.attachments (topicheaders_id);
CREATE TABLE wos.publishers (
	id serial NOT NULL DEFAULT nextval('wos.publishers_id_seq'::regclass),
	publ varchar(5) NOT NULL,
	name varchar(150) DEFAULT NULL::character varying,
	label_id int4
);
ALTER TABLE wos.publishers ADD CONSTRAINT publishers_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX publishers_publ_key ON wos.publishers (publ);
CREATE INDEX wos_publishers_label_id_ndx ON wos.publishers (label_id);
CREATE TABLE wos.specials (
	id int2 NOT NULL,
	text varchar(50) NOT NULL
);
ALTER TABLE wos.specials ADD CONSTRAINT specials_pkey PRIMARY KEY(id);
CREATE UNIQUE INDEX specials_text_key ON wos.specials (text);
CREATE TABLE wos.types (
	id int4 NOT NULL,
	code bpchar(2) DEFAULT NULL::bpchar,
	name varchar(150) DEFAULT NULL::character varying,
	entrytype_id int2
);
ALTER TABLE wos.types ADD CONSTRAINT types_pkey PRIMARY KEY(id);
CREATE INDEX wos_types_entrytype_id_ndx ON wos.types (entrytype_id);
CREATE TABLE wos.authors (
	id int4 NOT NULL,
	topicheaders_id int4,
	feature_id int4 NOT NULL
);
ALTER TABLE wos.authors ADD CONSTRAINT authors_pkey PRIMARY KEY(id);
CREATE INDEX wos_authors_feature_id_ndx ON wos.authors (feature_id);
CREATE INDEX wos_authors_topicheaders_id_ndx ON wos.authors (topicheaders_id);
CREATE TABLE wos.topicheaders (
	id int4 NOT NULL,
	title varchar(150) NOT NULL,
	publisher_id int4,
	publisher2_id int4,
	publisher3_id int4,
	type_id int4,
	fglinkt_id int4,
	fglinkx_id int4,
	orgprice numeric(5, 2) NOT NULL,
	repub2price numeric(5, 2) NOT NULL,
	repub3price numeric(5, 2) NOT NULL,
	ratings bpchar(4) DEFAULT NULL::bpchar,
	diskprice numeric(5, 2) NOT NULL,
	comment varchar(500) DEFAULT NULL::character varying,
	FGTKEY varchar(150) NOT NULL,
	entry_id int4,
	label_id int4,
	release_seq int2,
	wos_special_id int2
);
ALTER TABLE wos.topicheaders ADD CONSTRAINT topicheaders_pkey PRIMARY KEY(id);
CREATE INDEX wos_topicheaders_entry_idrelease_seq_ndx ON wos.topicheaders (entry_id,release_seq);
CREATE INDEX wos_topicheaders_fglinkt_id_ndx ON wos.topicheaders (fglinkt_id);
CREATE INDEX wos_topicheaders_fglinkx_id_ndx ON wos.topicheaders (fglinkx_id);
CREATE INDEX wos_topicheaders_label_id_ndx ON wos.topicheaders (label_id);
CREATE INDEX wos_topicheaders_publisher2_id_ndx ON wos.topicheaders (publisher2_id);
CREATE INDEX wos_topicheaders_publisher3_id_ndx ON wos.topicheaders (publisher3_id);
CREATE INDEX wos_topicheaders_publisher_id_ndx ON wos.topicheaders (publisher_id);
CREATE INDEX wos_topicheaders_type_id_ndx ON wos.topicheaders (type_id);
CREATE INDEX wos_topicheaders_wos_special_id_ndx ON wos.topicheaders (wos_special_id);
CREATE TABLE wos.topicrefs (
	id serial NOT NULL DEFAULT nextval('wos.topicrefs_id_seq'::regclass),
	topicheaders_id int4,
	referencetype_id int2,
	magazine_id int2,
	issue_yymm bpchar(4) DEFAULT NULL::bpchar,
	pagenumber numeric(5, 1) DEFAULT NULL::numeric,
	issue_id int4,
	feature_id int4
);
ALTER TABLE wos.topicrefs ADD CONSTRAINT topicrefs_pkey PRIMARY KEY(id);
CREATE INDEX wos_topicrefs_feature_id_ndx ON wos.topicrefs (feature_id);
CREATE INDEX wos_topicrefs_issue_id_ndx ON wos.topicrefs (issue_id);
CREATE INDEX wos_topicrefs_magazine_id_ndx ON wos.topicrefs (magazine_id);
CREATE INDEX wos_topicrefs_referencetype_id_ndx ON wos.topicrefs (referencetype_id);
CREATE INDEX wos_topicrefs_topicheaders_id_ndx ON wos.topicrefs (topicheaders_id);
ALTER TABLE wos.attachments ADD CONSTRAINT fk_wos_attachment_topicheader FOREIGN KEY (topicheaders_id) REFERENCES wos.topicheaders(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE wos.attachments ADD CONSTRAINT fk_wos_attachment_feature FOREIGN KEY (feature_id) REFERENCES zxdb.features(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE wos.publishers ADD CONSTRAINT fk_wos_publisher_label FOREIGN KEY (label_id) REFERENCES zxdb.labels(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE wos.types ADD CONSTRAINT fk_wos_type_entrytype FOREIGN KEY (entrytype_id) REFERENCES zxdb.entrytypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE wos.authors ADD CONSTRAINT fk_wos_author_topicheader FOREIGN KEY (topicheaders_id) REFERENCES wos.topicheaders(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE wos.authors ADD CONSTRAINT fk_wos_author_feature FOREIGN KEY (feature_id) REFERENCES zxdb.features(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE wos.topicheaders ADD CONSTRAINT Pub1 FOREIGN KEY (publisher_id) REFERENCES wos.publishers(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE wos.topicheaders ADD CONSTRAINT Pub2 FOREIGN KEY (publisher2_id) REFERENCES wos.publishers(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE wos.topicheaders ADD CONSTRAINT Pub3 FOREIGN KEY (publisher3_id) REFERENCES wos.publishers(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE wos.topicheaders ADD CONSTRAINT fk_wos_topicheader_special FOREIGN KEY (wos_special_id) REFERENCES wos.specials(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE wos.topicheaders ADD CONSTRAINT Type FOREIGN KEY (type_id) REFERENCES wos.types(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE wos.topicheaders ADD CONSTRAINT fk_wos_topicheader_entry FOREIGN KEY (entry_id) REFERENCES zxdb.entries(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE wos.topicheaders ADD CONSTRAINT fk_wos_topicheader_label FOREIGN KEY (label_id) REFERENCES zxdb.labels(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE wos.topicheaders ADD CONSTRAINT fk_wos_topicheader_release FOREIGN KEY (entry_id,release_seq) REFERENCES zxdb.releases(entry_id,release_seq) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE wos.topicrefs ADD CONSTRAINT TopicHeaders FOREIGN KEY (topicheaders_id) REFERENCES wos.topicheaders(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE wos.topicrefs ADD CONSTRAINT fk_wos_topicref_feature FOREIGN KEY (feature_id) REFERENCES zxdb.features(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE wos.topicrefs ADD CONSTRAINT fk_wos_topicref_issue FOREIGN KEY (issue_id) REFERENCES zxdb.issues(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE wos.topicrefs ADD CONSTRAINT fk_wos_topicref_magazine FOREIGN KEY (magazine_id) REFERENCES zxdb.magazines(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE wos.topicrefs ADD CONSTRAINT fk_wos_topicref_type FOREIGN KEY (referencetype_id) REFERENCES zxdb.referencetypes(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
