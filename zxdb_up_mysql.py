'''
Created on 11 de nov de 2016

@author: lisias
'''

import os, subprocess

PASSWD = None # trocar para '100senha' nos RPis.

JOBS = [
	('root', PASSWD, 'SQL/mysql/0000_CLEAR_DB.sql'),
	('root', PASSWD, 'RAW/zxdb/ZXDB_latest_mysql.sql'),
	('root', PASSWD, 'SQL/mysql/9998_CREATE_USERS.sql'),
	('root', PASSWD, 'SQL/mysql/9999_APPLY_ROLES.sql'),
]

MYSQL_CMD = '/Applications/XAMPP/bin/mysql' # '/usr/bin/mysql'

def apply(role : str, pwd : str, file : str):
	if not os.path.exists(file):
		raise Exception("Not found: %s !" % (file))

	print ("%s : " % file, end='')

	with open(file, 'r') as my_stdin, open(file + ".out", 'w') as my_stdout, open(file + ".err", 'w') as my_stderr :
		my_stdout.write("")
		my_stderr.write("")
		cmd  = (MYSQL_CMD, '-u', role, '-p%s'%pwd, 'zxdb') if pwd else (MYSQL_CMD, '-u', role, 'zxdb')
		rcode = subprocess.call(cmd, stdin = my_stdin, stdout=my_stdout, stderr=my_stderr)

	print (rcode)

if __name__ == '__main__':
	for j in JOBS:
		apply(j[0], j[1], j[2])
