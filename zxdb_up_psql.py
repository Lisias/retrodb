'''
Created on 11 de nov de 2016

@author: lisias
'''

import os, subprocess

JOBS = [
	('root',    'SQL/psql/0000_CREATE_DATABASE.sql'),
	('admin',   'SQL/psql/0010_CREATE_SCHEMAS.sql'),
	('admin',   'SQL/psql/0020_CREATE_USERS.sql'),
	('admin',   'SQL/psql/0030_APPLY_ROLES.sql'),

	('admin',   'SQL/psql/zxdb/ZXDB_latest_0000_create.sql'),    ('admin',   'SQL/psql/0030_APPLY_ROLES.sql'),
	('manager', 'SQL/psql/zxdb/ZXDB_latest_0010_*_populate_*.sql'),
]

PSQL_CMD = '/Applications/Postgres.app/Contents/Versions/9.6/bin/psql' #'/opt/local/bin/psql' # '/usr/bin/psql'

def apply(role : str, file: str):
	if not os.path.exists(file):
		raise Exception("Not found: %s !" % (file))

	db = 'retrodb' if 'root' != role else 'postgres'
	user = "%s-%s" % (db, role) if 'root' != role else 'postgres'

	print ("%s : " % file, end='')

	with open(file, 'r') as my_stdin, open(file + ".out", 'w') as my_stdout, open(file + ".err", 'w') as my_stderr :
		my_stdout.write("")
		my_stderr.write("")
		rcode = subprocess.call((PSQL_CMD, db, user), stdin = my_stdin, stdout=my_stdout, stderr=my_stderr)

	print (rcode)

import glob
def apply_batch(role : str, mask: str):
	tasks = sorted(glob.glob(mask))
	for task in tasks:
		apply(role, task)

if __name__ == '__main__':
	for j in JOBS:
		apply_batch(j[0], j[1])
